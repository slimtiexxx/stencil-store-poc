import { createStore } from "@stencil/store";

export const createStaticStore = (component, propNames) => {
  const staticProperties = {
    frameworkCallback: component.frameworkCallback,
    frameworkProp: component.frameworkProp,
    ...propNames.reduce((acc, curr) => {
      acc[curr] = component[curr]

      return acc;
    }, {})
  };

  return createStore(staticProperties, () => false).state
}
