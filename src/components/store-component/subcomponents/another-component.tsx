import { h } from "@stencil/core";
import { store } from '../store';
import { increaseCount } from "../actions";

export const AnotherComponent = () => (
  <div>
    <p>The current count is {store.count}</p>
    <button onClick={increaseCount}>Increase count</button>
  </div>
)
