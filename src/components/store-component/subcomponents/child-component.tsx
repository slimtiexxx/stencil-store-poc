import { h } from "@stencil/core";
import { services, store } from "../store";

export const ChildComponent = () => (
  <div>
    <h1 onClick={services.openPageCallback}>Hello, my name is {store.first} {store.middle} {store.last}!</h1>
    <div>
      <p>Deep object state change: <strong>{store.deepObject.someValue ? 'true' : 'false'}</strong></p>
      <button onClick={() => store.deepObject.someValue = !store.deepObject.someValue}>Change!</button>
    </div>
  </div>
)
