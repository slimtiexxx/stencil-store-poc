import { store } from './store';

export const increaseCount = () => {
  store.count += 1;
}
