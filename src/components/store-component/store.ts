import { createStore } from "@stencil/store";

export const initStore = (component: any, initialState) => {
  /**
   * State will contain values, that will listen for change
   **/
  store = createStore(initialState).state;

  /**
   * Store can contain Framework callbacks and static properties
  **/
  services = {
    frameworkProp: component.frameworkProp,
    frameworkCallback: component.frameworkCallback,
  };
}

export let store;
export let services;
