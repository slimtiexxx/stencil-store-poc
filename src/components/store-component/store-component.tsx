import { Component, h, Prop } from '@stencil/core';
import { ChildComponent } from './subcomponents/child-component';
import { AnotherComponent } from "./subcomponents/another-component";
import { initStore } from "./store";

@Component({
  tag: 'store-component',
  shadow: true,
})
export class StoreComponent {
  // Framework props
  @Prop() frameworkCallback;
  @Prop() frameworkProp;

  componentWillLoad() {
    initStore(
      this,
      {
        count: 1,
        first: 'Stencil',
        middle: 'STORE',
        last: 'Component',
        deepObject: {
          someValue: true
        }
      }
    )
  }

  render() {
    console.log('rendered')
    return(
      <div>
        <ChildComponent />
        <AnotherComponent />
      </div>
    )
  }
}
