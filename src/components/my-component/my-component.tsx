import {Component, State, h, Prop} from '@stencil/core';
import { ChildComponent } from './subcomponents/child-component/child-component';
import { AnotherComponent } from "./subcomponents/another-component/another-component";

@Component({
  tag: 'my-component',
  shadow: true,
})
export class MyComponent {
  // Framework props
  @Prop() frameworkCallback;
  @Prop() frameworkProp;

  // Static props
  staticProperty = 'Static value'

  // States
  @State() first = 'Stencil';
  @State() middle: 'Boring';
  @State() last: 'Component';
  @State() count: number = 0;

  // Actions
  increaseCount() {
    this.count = this.count + 1;
  }

  // Store
  get store() {
    return {
      frameworkCallback: this.frameworkCallback,
      frameworkProp: this.frameworkProp,
      staticProperty: this.staticProperty,
      increaseCount: this.increaseCount.bind(this),
      count: this.count,
      first: this.first,
      middle: this.middle,
      last: this.last,
    }
  }

  render = () => [
    <ChildComponent {...this.store} />,
    <AnotherComponent {...this.store} />
  ]
}
