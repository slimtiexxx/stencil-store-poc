import { h } from "@stencil/core";

export const ChildComponent = (store) => {
  return (
    <h1>Hello, my name is {store.first} {store.middle} {store.last}!</h1>
  )
}
