import { h } from "@stencil/core";

export const AnotherComponent = (store) => (
  <div>
    <p>The current count is {store.count}</p>
    <button onClick={store.increaseCount}>Increase count</button>
  </div>
)
